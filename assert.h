#ifndef __ASSERT_H
#define __ASSERT_H

#if defined(NDEBUG)
# define assert(x)	((void)0)
#else
# define assert(x)			\
	do {				\
		if (x)			\
			break;		\
		__assert_fail(#x,	\
			__FILE__,	\
			__LINE__,	\
			__func__);	\
	} while (0)
#endif /* NDEBUG */

void __assert_fail(const char *expr,
		   const char *file,
		   unsigned int line,
		   const char *func);

#endif /* __ASSERT_H */

