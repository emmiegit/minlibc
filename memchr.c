#include "string.h"

void *memchr(const void *_buf, int ch, size_t n)
{
	const char *buf;
	size_t i;

	buf = _buf;
	for (i = 0; i < n; i++) {
		if (buf[i] == ch)
			return (void *)_buf;
	}
	return NULL;
}

