#ifndef __SIGNAL_H
#define __SIGNAL_H

#include <sys/types.h>

int raise(int signum);
int kill(pid_t pid, int signum);

#endif /* __SIGNAL_H */

