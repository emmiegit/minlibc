#include "string.h"

size_t strnlen(const char *str, size_t n)
{
	size_t i;

	for (i = 0; i < n && str[i]; i++)
		;
	return i;
}

