#include "string.h"

void *memcpy(void *_dest, const void *_src, size_t n)
{
	const char *src;
	char *dest;
	size_t i;

	src = _src;
	dest = _dest;
	for (i = 0; i < n; i++)
		dest[i] = src[i];
	return dest;
}

