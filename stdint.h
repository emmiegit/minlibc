#ifndef __STDINT_H
#define __STDINT_H

#include "stddef.h"

#define CHAR_BIT		8
#define SCHAR_MIN		(-(1 << 7))
#define SCHAR_MAX		((1 << 7) - 1)
#define UCHAR_MAX		((1 << 8) - 1)

typedef signed char		int8_t;
typedef unsigned char		uint8_t;
typedef signed short		int16_t;
typedef unsigned short		uint16_t;
typedef signed int		int32_t;
typedef unsigned int		uint32_t;
typedef signed long int		int64_t;
typedef unsigned long int	uint64_t;

#define INT8_MIN		(-(1 << 7))
#define INT8_MAX		((1 << 7) - 1)
#define UINT8_MAX		((1 << 8) - 1)

#define INT16_MIN		(-(1 << 15))
#define INT16_MAX		((1 << 15) - 1)
#define UINT16_MAX		((1 << 16) - 1)

#define INT32_MIN		(-(1 << 31))
#define INT32_MAX		((1 << 31) - 1)
#define UINT32_MAX		((1 << 32) - 1)

#define INT64UL_MIN		(-(1 << 63L))
#define INT64UL_MAX		((1 << 63L) - 1)
#define UINT64UL_MAX		((1 << 64) - 1)

typedef signed char		int_least8_t;
typedef unsigned char		uint_least8_t;
typedef signed short		int_least16_t;
typedef unsigned short		uint_least16_t;
typedef signed int		int_least32_t;
typedef unsigned int		uint_least32_t;
typedef signed long int		int_least64_t;
typedef unsigned long int	uint_least64_t;

#define INT_LEAST8_MIN		(-(1 << 7))
#define INT_LEAST8_MAX		((1 << 7) - 1)
#define UINT_LEAST8_MAX		((1 << 8) - 1)

#define INT_LEAST16_MIN		(-(1 << 15))
#define INT_LEAST16_MAX		((1 << 15) - 1)
#define UINT_LEAST16_MAX	((1 << 16) - 1)

#define INT_LEAST32_MIN		(-(1 << 31))
#define INT_LEAST32_MAX		((1 << 31) - 1)
#define UINT_LEAST32_MAX	((1 << 32) - 1)

#define INT_LEAST64UL_MIN	(-(1 << 63L))
#define INT_LEAST64UL_MAX	((1 << 63L) - 1)
#define UINT_LEAST64UL_MAX	((1 << 64) - 1)

typedef signed char		int_fast8_t;
typedef unsigned char		uint_fast8_t;
typedef signed long int		int_fast16_t;
typedef unsigned long		uint_fast16_t;
typedef signed long int		int_fast32_t;
typedef unsigned long		uint_fast32_t;
typedef signed long int		int_fast64_t;
typedef unsigned long		uint_fast64_t;

#define INT_FAST8_MIN		(-(1 << 7))
#define INT_FAST8_MAX		((1 << 7) - 1)
#define UINT_FAST8_MAX		((1 << 8) - 1)

#define INT_FAST16_MIN		(-(1 << 63L))
#define INT_FAST16_MAX		((1 << 63L) - 1)
#define UINT_FAST16_MAX		((1 << 64UL) - 1)

#define INT_FAST32_MIN		(-(1 << 63L))
#define INT_FAST32_MAX		((1 << 63L) - 1)
#define UINT_FAST32_MAX		((1 << 64UL) - 1)

#define INT_FAST64UL_MIN	(-(1 << 63L))
#define INT_FAST64UL_MAX	((1 << 63L) - 1)
#define UINT_FAST64UL_MAX	((1 << 64) - 1)

typedef signed long int		intmax_t;
typedef unsigned long int	uintmax_t;

typedef signed long int		intptr_t;
typedef unsigned long int	uintptr_t;
typedef signed long int		ptrdiff_t;

#define INTPTR_MIN		(-(1 << 15) - 1)
#define INTPTR_MAX		((1 << 15) - 1)
#define UINTPTR_MAX		((1 << 16) - 1)

#define INTMAX_MIN		(-(1 << 63L))
#define INTMAX_MAX		((1 << 63L) - 1)
#define UINTMAX_MAX		((1 << 64UL) - 1)

#endif /* __STDINT_H */

