#ifndef __STRING_H
#define __STRING_H

#include "stddef.h"

char *strcpy(char *dest, const char *src);
char *strncpy(char *dest, const char *src, size_t n);
size_t strlen(const char *str);
size_t strnlen(const char *str, size_t maxlen);
char *strchr(const char *str, int ch);
char *strrchr(const char *str, int ch);

void *memcpy(void *dest, const void *src, size_t n);
void *memmove(void *dest, const void *src, size_t n);
void *memchr(const void *buf, int ch, size_t n);
void *memrchr(const void *buf, int ch, size_t n);
void *memset(void *buf, int ch, size_t n);

#endif /* __STRING_H */

