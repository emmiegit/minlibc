### minutils

A minimalist implementation the standard C library. Doesn't support
new fancy features like locales, just the basics.

Assumes you are using Linux with development headers, 64-bit only.
Makes a lot assumptions about types, like that `unsigned long` is 64-bit.

This project is licensed under the WTFPL.

