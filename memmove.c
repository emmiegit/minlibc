#include "stdint.h"
#include "string.h"

void *memmove(void *_dest, const void *_src, size_t n)
{
	const char *src;
	char *dest;
	ptrdiff_t diff;
	size_t i;

	src = _src;
	dest = _dest;

	diff = dest - src;
	if (diff == 0)
		return dest;

	if (diff < 0)
		for (i = 0; i < n; i++)
			dest[i] = src[i];
	else
		for (i = 0; i < n; i++)
			dest[n - i - 1] = src[n - i - 1];
	return dest;
}

