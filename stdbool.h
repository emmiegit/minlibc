#ifndef __STDBOOL_H
#define __STDBOOL_H

typedef unsigned char bool;

#undef true
#define true	1

#undef false
#define false	0

#endif /* __STDBOOL_H */

