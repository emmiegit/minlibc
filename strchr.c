#include "string.h"

char *strchr(const char *str, int ch)
{
	size_t i;

	for (i = 0; str[i]; i++) {
		if (str[i] == ch)
			return (char *)str;
	}
	return NULL;
}

