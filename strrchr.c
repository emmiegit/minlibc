#include "string.h"

char *strrchr(const char *str, int ch)
{
	size_t i, n;

	for (n = 0; str[n]; n++)
		;
	for (i = 0; i < n; i++) {
		if (str[n - i - 1] == ch)
			return (char *)str;
	}
	return NULL;
}

