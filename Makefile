.SUFFIXES:
.PHONY: all force clean

SOURCES := $(wildcard *.c)
OBJECTS := $(patsubst %.c,bin/%.o,$(SOURCES))
PIC_OBJ := $(patsubst %.c,bin/%.pic.o,$(SOURCES))
LIBRARY := libmin_c

FLAGS   := -std=c99 -pedantic -I. -Wall -Wextra -Wshadow -Wmissing-prototypes -nostdlib

AR_DEST := bin/$(LIBRARY).a
SO_DEST := bin/$(LIBRARY).so

all: CFLAGS += -Os
all: bin $(AR_DEST) $(SO_DEST)

debug: CFLAGS += -g
debug: bin $(AR_DEST) $(SO_DEST)

bin:
	@echo '[MD] bin'
	@mkdir -p bin

bin/%.pic.o: %.c
	@echo '[CC] $(@F)'
	@$(CC) $(FLAGS) $(CFLAGS) -fpic -c -o $@ $<

bin/%.o: %.c
	@echo '[CC] $(@F)'
	@$(CC) $(FLAGS) $(CFLAGS) -c -o $@ $<

$(AR_DEST): $(OBJECTS)
	@echo '[AR] $(@F)'
	@$(AR) rcs $@ $^

$(SO_DEST): $(PIC_OBJ)
	@echo '[LD] $(@F)'
	@$(CC) $(FLAGS) $(CFLAGS) -shared -o $@ $^

clean:
	@echo '[RMDIR] bin'
	@rm -rf bin

