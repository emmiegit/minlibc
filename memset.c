#include "string.h"

void *memset(void *_buf, int ch, size_t n)
{
	char *buf;
	size_t i;

	buf = _buf;
	for (i = 0; i < n; i++)
		buf[i] = ch;
	return _buf;
}

